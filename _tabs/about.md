---
# the default layout is 'page'
icon: fas fa-info-circle
order: 4
---

Hi, I'm Cooper! I have an interest in Linux as well as free and open source software.

Feel free to reach out to me via email and [matrix](https://matrix.org/):

* Email: [memoryfile@pm.me](mailto:memoryfile <memoryfile@pm.me>)
* matrix: [@memoryfile:matrix.org](https://matrix.to/#/@memoryfile:matrix.org)

You can also find me on [GitHub](https://github.com/memoryfile) and [Codeberg](https://codeberg.org/memoryfile).
